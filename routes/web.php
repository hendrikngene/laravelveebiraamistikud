<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * @see  https://laravel.com/docs/5.3/routing
 */

Route::get('/', function () {
    return view('welcome');
});

/**
 * Algsed katsetused
 */
Route::get('/test', function() {
    return 'Hello World'; // Tagasta tavaline string
});

Route::get('/viewtest', function() {
    $people = ['Hendrik', 'Andreas', 'Gert'];

    // Kõik neli tehniliselt korrektset viisi, kuidas andmeid vaadetele edasi anda
    // return view('test')->with('people', $people);
    // return view('test', ['people' => $people]);
    // return view('test')->withPeople($people);
    return view('test', compact('people')); // Tagasta vaade failis resources/views/test.blade.php
});

/**
 * Kontrollerisse marsruutimine
 * Kontrolleri loomine käsuga php artisan make:controller <nimi>
 * Asub failis app/Http/Controllers/<nimi>
 */
Route::get('/posts', 'PostsController@index');
Route::get('/posts/view/{id}', 'PostsController@view');

// Ainult sisse logitud kasutajad saavad uusi postitusi lisada
Route::group(['middleware' => ['auth']], function() {
    Route::get('/posts/create', 'PostsController@create');
    Route::post('/posts/create', 'PostsController@store');
});

Auth::routes();
Route::get('/home', 'HomeController@index');
