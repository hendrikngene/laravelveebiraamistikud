#Laravel

## Dokumentatsioon

Dokumentatsiooni leiab [Laravel-i veebisaidit](http://laravel.com/docs).

## Installimine

[Vaata installimise juhendeid](installing.md)

## Tunnis tehtud näide

Tõmmake endale projekt alla git-ga või download menüüst. Viige projekt õigesse kausta ning avage terminal ja kirjutage käsk

    composer install

Andmebaasi tabelite tegemiseks tehke **.env.example** failist koopia nimega **.env** ning pange enda andmebaasi nimi, kasutajanimi ning parool. Minge projekti juurkataloogi ja sisestage käsk

```
php artisan migrate
```