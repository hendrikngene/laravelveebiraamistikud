{{-- https://laravel.com/docs/5.3/blade --}}
@extends('layouts.layout') {{-- Võtame ülejäänud HTML-i failist resources/views/layouts/layout.blade.php --}}

@section('title')
    Test
@stop

@section('content')
    <h3>This is where the content will go.</h3>
    @if (isset($people))
        There are people
    @else
        There are no people
    @endif

    <ul>
    @foreach($people as $person)
        <li>{{ $person }}</li>
    @endforeach
    </ul>
@stop