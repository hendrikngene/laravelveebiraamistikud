@extends('layouts.layout')

@section('content')
    <h1>{{ $post->title }}</h1>
    <h3>{{ $post->user->name }}</h3>
    <div>{{ $post->content }}</div>
@stop