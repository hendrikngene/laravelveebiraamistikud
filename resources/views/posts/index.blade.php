@extends('layouts.layout')

@section('content')
    <h1>This is where the posts will appear</h1>
    <ul>
        @foreach ($posts as $post)
            <li><a href="/posts/view/{{ $post->id }}">{{ $post->title }}</a></li>
        @endforeach
    </ul>
@stop