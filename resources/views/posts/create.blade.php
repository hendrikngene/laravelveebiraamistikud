@extends('layouts.layout')

@section('content')
    <form action="/posts/create" method="POST">
        {{ csrf_field() }} {{-- https://laravel.com/docs/5.3/csrf --}}
        <p>Title:</p>
        <input type="text" name="title">
        <p>Content:</p>
        <textarea name="content"></textarea>
        <input type="submit" value="Submit">
    </form>
@stop