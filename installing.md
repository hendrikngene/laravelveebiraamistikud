# Installimine

## Windows

[Laragon](https://laragon.org/download.html)

Tunnis kasutasime Lite+Apache versiooni, kuid nendel väga vahet ei ole. Valige endale kõige sobivam variant. Kui Apache mingil põhjusel ei tööta ja viga leida ei viitsi, siis saab kasutada ka laravel-i oma serverit. Selleks minge projekti juurkataloogi ja sisestaga käsk

    php artisan serve
    
Nüüd peaks olema projekt ligipääsetav aadressilt localhost:8000

## Linux/OS X

Installige  [VirtualBox](https://www.virtualbox.org/wiki/Downloads) ja [Vagrant](https://www.vagrantup.com/downloads.html). Täpsemad juhised [https://laravel.com/docs/5.3/homestead](https://laravel.com/docs/5.3/homestead).

## Vagrant Box-i tegemine
Avage Terminal ja kirjutage käsk

    vagrant box add laravel/homestead

Homestead-i installimine

    cd ~
    git clone https://github.com/laravel/homestead.git Homestead

Kui repositoorium on kloonitud, siis tuleks confi failide tekitamiseks kasutada käsklust

    cd Homestead
    bash init.sh

Järgmiseks avage **Homestead.yaml** fail, mis asub kaustas ~/.homestead/Homestead.yaml ning tehke umbes sarnased muudatused.

```
ip: "192.168.10.10"
memory: 2048
cpus: 1
provider: virtualbox
authorize: ~/.ssh/id_rsa.pub
keys:
    - ~/.ssh/id_rsa

# Kaust, kuhu hakkavad tekkima Laravel-i projektid
folders:
    - map: ~/Projects # Kaust, mis asub sinu arvutis. (loo kui sellist kausta pole)
    - to: /home/vagrant/Projects # Kaust, mis tekkib virtuaalmasinasse
# Laravel-i projektid
sites:
    - map: homestead.app # Veebiaadress, millega saad brauserist ligi
    - to: /home/vagrant/Projects/projektinimi/public # Loodav projekt
# Andmebaasid, mis luuakse
databases:
    - homestead
```

Nüüd tuleks muuta faili /etc/hosts, et me saaks projektile ip asemel nimega ligi.

    sudo nano /etc/hosts
Ja lisada faili lõppu

    192.168.10.10 homestead.app

SSH võtme genereerimine

    ssh-keygen -t rsa -C "your_email@example.com"

Vagrant box-i käivitamine

    cd ~/Homestead
    vagrant up

Projekti loomine

    vagrant ssh
    cd Projects # Kaust Homestead.yaml failist
    laravel new projektinimi

Nüüd peaks olema rakendus ligipääsetav aadressilt homestead.app
