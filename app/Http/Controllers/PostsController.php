<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::all(); // Otsime üles kõik postitused

        return view('posts.index', compact('posts')); // Tagasta vaade failist resources/views/posts/index.blade.php
    }

    public function view($postId)
    {
        // Võtame kaasa ka kasutaja, mitte ainult user_id, user sõna tuleb Post modeli meetodist user
        $post = Post::with('user')->find($postId);

        // return $post; // Tahame näha, mis andmed me saime
        return view('posts.view', compact('post')); // Tagastame vaate failist resources/views/posts/view.blade.php
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $post = new Post;
        $post->title = $request->title;
        $post->content = $request->content;
        $post->user_id = Auth::user()->id;
        $post->save(); // Salvestame kirje andmebaasi

        return redirect('/posts');
    }
}
