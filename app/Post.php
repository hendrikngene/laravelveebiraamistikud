<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * Laravel's Eloquent ORM
     * @see https://laravel.com/docs/5.3/eloquent
     * @see https://laravel.com/docs/5.3/eloquent-relationships
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
